страница showcase (витрина):

1. инициализационная установка стейта.

- фетчим товары из БД (firebase)
- записываем загруженные товары в store.products

- берем корзину из localstorage и, если она не пустая, проверяем наличие корзинных товаров с товарами в store. Если в свежеполученных товарах нет таких корзинных товаров, удаляем из корзины эти товары (чтобы в заказ не ушли товары уже пропавшие из БД)
- записываем проверенную корзину (товары, сумма) в store.cart

- обновляем localstorage

2. инициализационная отрисовка и установка listeners.

- устанавливаем глобальные переменные HeaderCartTotal и HeaderCartCounter
- записываем в их innerHTML данные из store

- устанавливаем глобальную переменную mainContainer
- map() cart.products (array) в cardsList (array)
- reduce() cardsList (array) в content (string)
- записываем content в mainContainer.innerHTML

- устанавливаем в глобальные переменные cardContainers (array)
- добавляем listeners внутри каждого cardContainer на кнопки "+" и "-" со следующей логикой:
  -- диспатч в store.cart addCartProduct(code) или removeCartProduct(code) (редьюсер в store см. ниже)
  -- установка в cardCounter, HeaderCartTotal и HeaderCartCounter новых значений из storeCart
  -- установка класса hide для кнопки - в зависимости от store
  -- обновление localstorage

  - установка логики для модального окна (увеличение картинки) - пока шаблон из bootstrap

3. редьюсеры (методы) в store

- геттеры (селекторы)
- сеттеры товаров и корзины
- добавление и уменьшение в корзине - принимает код товара, изменяет количество товара в cart или добавляет/удаляет товар, изменяет сумму корзины

страница cart (корзина):

1. инициализационная установка стейта.

// в store пусто, т.к. другая страница; в local storage может быть корзина (товары { код, наименование, кол-во, цена} и сумма), или пусто, если корзина пустая

- загружаем LS и диспатчим в store.cart

2. инициализационная отрисовка и установка listeners.

- устанавливаем глобальную переменную tableTotal и записываем в innerHTML данные из store

- устанавливаем глобальную переменную mainContainer
- map() cart.products (array) в productsDataList (array)
- reduce() productsDataList (array) в content (string)
- записываем content в mainContainer.innerHTML

- устанавливаем в глобальные переменные productContainers (array)
- добавляем listeners внутри каждого productContainer на кнопки "+" и "-" со следующей логикой:
  -- диспатч в store.cart addCartProduct(code) или removeCartProduct(code)
  -- установка в tableTotal, rowCounter новых значений из store (+ удаление строки при 0 количестве)
  -- обновление localstorage

- установка логики для Submit формы:
  -- проверка на наличие товаров в корзине
  -- проверка на заполненность всех полей формы
  -- валидация email

  -- составление объекта order {customer, cartBody}
  -- диспатч order в store (пока просто так, для oneway flow)
  -- запушивание order в БД (firebase)

  -- диспатч пустой корзины в store
  -- обновление local storage

  -- запуск модального окна ("ваш заказ принят")
  ---- пока реализовано через костыль - добавление кнопке классов bootstrap для вызова модалки

- кнопка выход - просто переход на страницу витрины (корзина сохранена в local storage)
- модальное окно ("ваш заказ принят") при закрытии переходит на витрину
