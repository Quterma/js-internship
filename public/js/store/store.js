import uuid from "./../utils/uuid.js";

const store = {
	cart: {
		products: {
			// 	"code" : { title, price, code, quantity }
		},
		total: 0,
	},
	products: {
		// "code" : { title, price, code, description }
	},
	orders: {
		// customer: { firstName, lastName, email, phone	},
		// products: {},
		// total
	},
};

export const getStoreProducts = () => store.products;

export const setStoreProducts = newStoreProducts => (store.products = newStoreProducts);

export const getStoreCart = () => store.cart;

export const setStoreCart = newCart => (store.cart = newCart);

export const checkStoreCartBadCodes = () => {
	if (!store.cart.products || !store.products) return;
	const cartProducts = Object.keys(store.cart.products);
	const storeProducts = Object.keys(store.products);
	const badCodes = cartProducts.filter(x => !storeProducts.includes(x));
	badCodes.forEach(code => {
		store.cart.total -= Number(store.cart.products[code].price) * store.cart.products[code].quantity;
		delete store.cart.products[code];
	});
};

export const addToCart = code => {
	const cartProducts = store.cart.products;
	const { price, title } = store.products[code] || cartProducts[code];
	store.cart.total += Number(price);
	cartProducts[code] ? (cartProducts[code].quantity += 1) : (cartProducts[code] = { quantity: 1, price, code, title });
};

export const removeFromCart = code => {
	const cartProducts = store.cart.products;
	store.cart.total -= Number(cartProducts[code].price);
	cartProducts[code].quantity > 1 ? (cartProducts[code].quantity -= 1) : delete store.cart.products[code];
};

export const setStoreOrder = order => {
	const id = uuid();
	store.orders[id] = order;
};

export const clearStoreCart = () => {
	store.cart.products = {};
	store.cart.total = 0;
};

export const getStoreOrders = () => store.orders;
