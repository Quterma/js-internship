// добавляет в кнопку закрытия модального окна логику - по клику скрывает модальное окно и убирает overflow: hidden у body
export const setModalClose = () => {
	const modal = document.querySelector(".js_modalContainer");
	const btn = document.querySelector(".js_modalClose");
	function hide() {
		modal.style.display = "none";
		document.body.classList.remove("noScroll");
	}
	btn.addEventListener("click", hide);
	window.addEventListener("click", event => {
		if (event.target == modal) {
			hide();
		}
	});
};

// вспомогательная функция для показа модального окна
export const showModal = () => {
	const modal = document.querySelector(".js_modalContainer");
	document.body.classList.add("noScroll");
	modal.style.display = "block";
};
