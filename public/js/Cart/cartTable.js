import { setCartTotal } from "./../cartValues.js";
import { saveStorageState } from "./../localStorage.js";
import { addToCart, getStoreCart, removeFromCart } from "./../store/store.js";

// templating cart table
const сartTableRow = (cartProduct, index) => {
	const { title, price, code, quantity } = cartProduct;
	return `
    <tr class="js_rowContainer" data-id=${code}>
      <th scope="row" class="table__index">${index + 1}</th>
      <td class="table__title">${title}</td>
      <td class="table__code">${code}</td>
      <td class="table__price">${price}$</td>
      <td class="btn-group-sm me-2" role="group" aria-label="decrease increase buttons group">
        <button type="button" class="btn btn-outline-success js_removeFromCart">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-outline-success js_addToCart">
          <i class="fas fa-plus"></i>
        </button>
      </td>
      <td class="js_row__counter">${quantity}</td>
    </tr>
  `;
};

export const displayCartTable = container => {
	const storeCart = getStoreCart();

	if (!storeCart || !storeCart.products || Object.keys(storeCart.products).length === 0) return;

	// create array from cart.products object
	const productsDataList = Object.values(storeCart.products);

	// map data array into elements array
	const rowsList = productsDataList.map(сartTableRow);

	// reduce to string
	const content = rowsList.reduce((acc, curr) => acc + curr);

	container.innerHTML = content;
};

// вспомогательная функция для установки логики на кнопки в карточке
const setButtonLogic = (row, dispatch) => {
	const code = row.dataset.id;

	// - диспатч в store
	dispatch(code);

	// - обновляем localstorage (Cart)
	saveStorageState(getStoreCart());

	// - апдейт стейта tableTotal
	setCartTotal(".js_table__total");

	// - апдейт counter в строке
	const storeCart = getStoreCart();
	const quantity = storeCart.products[code] && storeCart.products[code].quantity;
	if (!quantity) {
		row.outerHTML = "";
		return;
	}
	const counter = row.querySelector(".js_row__counter");
	counter.innerHTML = quantity;
};

export const setRowLogic = row => {
	const plusButton = row.querySelector(".js_addToCart");
	const minusButton = row.querySelector(".js_removeFromCart");

	// установка listeners и зависимостей на кнопки
	plusButton.addEventListener("click", () => setButtonLogic(row, addToCart));
	minusButton.addEventListener("click", () => setButtonLogic(row, removeFromCart));
};
