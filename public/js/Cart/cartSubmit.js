import { getStoreCart, setStoreOrder, getStoreOrders, clearStoreCart } from "./../store/store.js";
import { saveStorageState } from "./../localStorage.js";
import { setFirebaseCollection } from "./../dal/fetching.js";
import { showModal } from "./../utils/modals.js";

const validateEmail = email => {
	const re = /\S+@\S+\.\S+/;
	return re.test(email);
};

export const setCartSubmitLogic = () => {
	const cartBody = getStoreCart();
	const cartForm = document.querySelector(".js_cart__form");
	cartForm.addEventListener("submit", async function (event) {
		event.preventDefault();

		// -- проверка на наличие товаров в корзине
		if (Object.keys(cartBody.products).length < 1) return;

		const firstName = this.elements.firstName.value;
		const lastName = this.elements.lastName.value;
		const email = this.elements.email.value;
		const phone = this.elements.phone.value;

		// -- проверка на заполненность всех полей формы
		if (!firstName || !lastName || !email || !phone) return;

		// -- валидация email
		if (!validateEmail(email)) return;

		// -- составление объекта order {customer, cartBody}
		const customer = { firstName, lastName, email, phone };
		const order = { customer, ...cartBody };

		// -- диспатч order в store (пока просто так, для oneway flow)
		setStoreOrder(order);
		console.log(getStoreOrders());

		// -- пуш order в БД (firebase)
		// setFirebaseCollection(db, "orders", order);

		// -- показ модального окна
		showModal();

		// -- диспатч пустой корзины в store
		clearStoreCart();

		// -- обновление local storage
		saveStorageState(getStoreCart());
	});
};
