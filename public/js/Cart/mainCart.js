import { loadStorageState } from "./../localStorage.js";
import { setStoreCart } from "./../store/store.js";
import { displayCartTable, setRowLogic } from "./cartTable.js";
import { setCartSubmitLogic } from "./cartSubmit.js";
import { setCartTotal } from "./../cartValues.js";

// after DOMContentLoaded
document.addEventListener("DOMContentLoaded", () => {
	// ===== 1. инициализационная установка стейта. =====

	// - загружаем LS и диспатчим в store.cart
	setStoreCart(loadStorageState());

	// ===== 2. инициализационная отрисовка и установка listeners. =====

	// - устанавливаем глобальную переменную tableTotal и записываем в innerHTML данные из store
	setCartTotal(".js_table__total");

	// 	устанавливаем контент (таблицу) в DOM
	const mainContainer = document.querySelector("#cartTableBody");
	displayCartTable(mainContainer);

	// 	- устанавливаем логику в строки таблицы
	const rowsList = document.querySelectorAll(".js_rowContainer");
	rowsList.forEach(row => setRowLogic(row));

	// 	- устанавливаем логику в Submit корзины
	setCartSubmitLogic();
});
