import { getStoreCart } from "./store/store.js";

export const setCartCounter = className => {
	const storeCart = getStoreCart();
	const headerCounterElement = document.querySelector(className);
	const quantity = storeCart && Object.keys(storeCart.products).length;
	headerCounterElement.innerHTML = quantity;
	quantity > 0 ? headerCounterElement.classList.remove("hide") : headerCounterElement.classList.add("hide");
};

export const setCartTotal = className => {
	const storeCart = getStoreCart();
	const total = storeCart && storeCart.total;
	const headerTotalElement = document.querySelector(className);
	headerTotalElement.innerHTML = total + "$";
};
