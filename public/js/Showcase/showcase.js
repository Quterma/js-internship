import { getStoreProducts, getStoreCart, removeFromCart, addToCart } from "./../store/store.js";
import { setCartCounter, setCartTotal } from "./../cartValues.js";
import { saveStorageState } from "./../localStorage.js";
import { showModal } from "./../utils/modals.js";

const cardElement = product => {
	const { title, price, description, code, quantity } = product;
	return `
    <div class="card card__container js_cardContainer" style="width: 18rem" data-id=${code}>
      <div class="card__img_wrapper">
				<span class="badge bg-warning text-dark js_card__counter card__counter">${quantity || ""}</span>
				<img
				src="https://loremflickr.com/320/240?random=${code}"
				class="card-img-top js_cardImg card__image"
				alt="product"
				onload="imgLoaded(this)"
				/>
      </div>
      <div class="card-header flex">
        <h4 class="card-title card__price">${price}$</h4>
        <button class="btn btn-outline-success card__button js_removeFromCart ${!quantity && "hide"}">
          <i class="fas fa-minus"></i>
        </button>
        <button class="btn btn-outline-success card__button js_addToCart">
          <i class="fas fa-plus"></i>
          <i class="fas fa-shopping-cart"></i>
        </button>
      </div>
      <div class="card-body">
        <h5 class="card-title">${title}</h5>
        <p class="card-text card__description">${description}</p>
        <p class="card-text small">Арт. <span class="card__code">${code}</span></p>
      </div>
    </div>
  `;
};

export const displayShowcase = container => {
	const storeProducts = getStoreProducts();
	const storeCart = getStoreCart();

	if (!storeProducts || Object.keys(storeProducts).length === 0) return;

	// create array from store.products object and combine data of the product with it's quantity
	const productsDataList = Object.values(storeProducts).map(product => {
		const quantity = storeCart.products[product.code] ? storeCart.products[product.code].quantity : 0;
		return { ...product, quantity };
	});

	// map data array into elements array
	const cardsList = productsDataList.map(cardElement);

	// reduce to string
	const content = cardsList.reduce((acc, curr) => acc + curr);

	container.innerHTML = content;
};

// вспомогательная функция для установки логики на кнопки в карточке
const setButtonLogic = (card, dispatch) => {
	const code = card.dataset.id;

	// - диспатч в store
	dispatch(code);

	// - обновляем localstorage (Cart)
	saveStorageState(getStoreCart());

	// - апдейт стейта HeaderCartTotal и HeaderCartCounter
	setCartCounter(".js_header__counter");
	setCartTotal(".js_header__total");

	// - апдейт counter карточки
	const storeCart = getStoreCart();
	const cartQuantity = storeCart.products[code] && storeCart.products[code].quantity;
	const counter = card.querySelector(".js_card__counter");
	counter.innerHTML = cartQuantity || "";

	// - hide/show элементов counter & minusButton карточки
	const minusButton = card.querySelector(".js_removeFromCart");
	[counter, minusButton].forEach(el => (cartQuantity ? el.classList.remove("hide") : el.classList.add("hide")));
};

// вспомогательная функция для установки логики на картинку-кнопку в карточке
const setImageLogic = card => {
	const code = card.dataset.id;
	const height = Math.floor(window.innerHeight * 0.6);
	const width = Math.floor(window.innerWidth * 0.6);
	const modalImage = document.querySelector(".js_modalImage");
	modalImage.src = `https://loremflickr.com/${width}/${height}?random=${code}`;
	showModal();
};

export const setCardLogic = card => {
	const plusButton = card.querySelector(".js_addToCart");
	const minusButton = card.querySelector(".js_removeFromCart");
	const image = card.querySelector(".js_cardImg");

	// установка открытия модалки по клику на картинку
	image.addEventListener("click", () => setImageLogic(card));

	// установка listeners и зависимостей на кнопки
	plusButton.addEventListener("click", () => setButtonLogic(card, addToCart));
	minusButton.addEventListener("click", () => setButtonLogic(card, removeFromCart));
};
