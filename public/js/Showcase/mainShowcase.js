import { getFirebaseProducts, fetchProducts } from "./../dal/fetching.js";
import { displayShowcase, setCardLogic } from "./showcase.js";
import { setStoreProducts, checkStoreCartBadCodes, getStoreCart, setStoreCart } from "./../store/store.js";
import { loadStorageState, saveStorageState } from "./../localStorage.js";
import { setCartCounter, setCartTotal } from "./../cartValues.js";
import { setModalClose } from "./../utils/modals.js";

// after DOMContentLoaded
document.addEventListener("DOMContentLoaded", async () => {
	// ===== 1. инициализационная установка стейта =====

	// - загружаем товары из БД (firebase) и записываем в store.products
	// const dbProducts = await getFirebaseProducts(db, "products");
	const dbProducts = await fetchProducts();

	// преобразуем массив в объект для удобства доступа (хмм...)
	const newStoreProducts = dbProducts.reduce((acc, curr) => ({ ...acc, [curr.code]: curr }), {});
	setStoreProducts(newStoreProducts);

	// - проверяем, есть ли в localstorage корзина, сверяем товары в корзине с товарами из БД и обновляем store.cart
	const storage = loadStorageState();
	storage && setStoreCart(storage);
	checkStoreCartBadCodes();

	// - обновляем localstorage (Cart)
	saveStorageState(getStoreCart());

	// ===== 2. инициализационная отрисовка и установка listeners =====

	// - устанавливаем стейт HeaderCartTotal и HeaderCartCounter
	setCartCounter(".js_header__counter");
	setCartTotal(".js_header__total");

	// 	устанавливаем контент (карточки) в DOM
	const mainContainer = document.querySelector(".js_showcaseContainer");
	displayShowcase(mainContainer);

	// 	- устанавливаем логику в карточки
	const cardsList = document.querySelectorAll(".js_cardContainer");
	cardsList.forEach(card => setCardLogic(card));

	// - логика для закрытия модального окна с картинкой
	setModalClose();
});
