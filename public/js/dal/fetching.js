// parsing products from DB
export const fetchProducts = async () => {
	try {
		const response = await fetch("./js/store/products.json");
		const data = await response.json();
		const products = data.products;
		return products;
	} catch (error) {
		console.log(error);
	}
};

export const getFirebaseProducts = async (db, collection) => {
	try {
		const snapshot = await db.collection(collection).get();
		const products = snapshot.docs.map(doc => doc.data());
		return products;
	} catch (error) {
		console.log(error);
	}
};

export const setFirebaseCollection = (db, collection, object) => {
	db.collection(collection).add(object);
};
